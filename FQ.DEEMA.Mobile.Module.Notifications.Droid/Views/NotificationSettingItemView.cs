﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Widget;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid.Views
{
    public class NotificationSettingItemView : LinearLayout
    {
        private Binding _switchBinding;
        private SwitchCompat _switchCompat;
        private TextView _textView;
        private WeakReferenceEx<NotificationSettingViewModel> _viewModelRef;

        public NotificationSettingItemView(Context context) : base(context)
        {
            Init(context);
        }

        public NotificationSettingItemView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        public NotificationSettingItemView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs,
            defStyle)
        {
            Init(context);
        }

        public NotificationSettingItemView(IntPtr handle, JniHandleOwnership owner) : base(handle, owner)
        {
        }

        private void Init(Context context)
        {
            Inflate(context, Resource.Layout.activity_notifications_settings_cell, this);

            _textView = FindViewById<TextView>(Resource.Id.activity_notifications_settings_cell_text_view);
            _switchCompat = FindViewById<SwitchCompat>(Resource.Id.activity_notifications_settings_cell_switch);
        }

        internal void Bind(NotificationSettingViewModel item)
        {
            _textView.Text = item.DisplayName;

            _viewModelRef = WeakReferenceEx.Create(item);

            _switchBinding?.Detach();
            _switchBinding = this.SetBinding(() => _viewModelRef.Target.IsEnabled, () => _switchCompat.Checked,
                BindingMode.TwoWay);
        }
    }
}