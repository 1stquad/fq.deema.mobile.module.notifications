﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Bindings.Droid;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.WhiteLabel.Droid;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid.Views
{
    [Activity]
    public class NotificationsSettingsActivity : ActivityBase<NotificationsSettingsViewModel>,
        ExpandableListView.IOnGroupClickListener
    {
        private ObservableGroupAdapter<string, NotificationSettingViewModel> _adapter;
        private BusyOverlayView _busyOverlayView;
        private NavigationBarView _navigationBarView;

        public bool OnGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long id)
        {
            return true;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_notifications_settings);

            _navigationBarView =
                FindViewById<NavigationBarView>(Resource.Id.activity_notifications_settings_navigation_bar);
            _navigationBarView.SetTitle(ViewModel.Title);
            _navigationBarView.SetLeftButton(StyleHelper.Style.NavigationBarBackButtonIcon, ViewModel.BackCommand);

            var listView = FindViewById<ExpandableListView>(Resource.Id.activity_notifications_settings_list_view);
            listView.SetOnGroupClickListener(this);

            _adapter = new ObservableGroupAdapter<string, NotificationSettingViewModel>(
                ViewModel.Settings,
                listView,
                (groupPosition, childPosition, isLastChild, convertView, parent, item) =>
                {
                    var itemView = convertView ?? new NotificationSettingItemView(parent.Context);
                    var notificationSettingItemView = itemView as NotificationSettingItemView;
                    notificationSettingItemView.Bind(item);
                    return notificationSettingItemView;
                },
                (groupPosition, isExpanded, convertView, parent, item) =>
                {
                    var itemView = convertView ?? LayoutInflater.From(parent.Context)
                        .Inflate(Resource.Layout.activity_notifications_settings_header, null);
                    var textView = itemView.FindViewById<TextView>(
                        Resource.Id.activity_notifications_settings_header_text_view);
                    textView.Text = item;
                    return itemView;
                });

            listView.SetAdapter(_adapter);

            _busyOverlayView = FindViewById<BusyOverlayView>(Resource.Id.activity_notifications_settings_busy_view);
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
            {
                _busyOverlayView.Visibility = BoolToViewStateConverter.ConvertGone(ViewModel.IsBusy);
            }));
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            _adapter.Dispose();
            _adapter = null;
        }
    }
}