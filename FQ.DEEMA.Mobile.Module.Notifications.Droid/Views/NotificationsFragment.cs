﻿using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using FQ.DEEMA.Mobile.Module.Notifications.Droid.ViewHolders;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Bindings.Droid;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.WhiteLabel.Droid;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;
using Softeq.XToolkit.WhiteLabel.Droid.Shared.Extensions;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid.Views
{
    public class NotificationsFragment : FragmentBase<NotificationsViewModel>
    {
        private ObservableRecyclerGroupViewAdapter<string, NotificationViewModel> _adapter;
        private BusyOverlayView _busyOverlayView;
        private NavigationBarView _navigationBarView;
        private RecyclerView _recyclerView;
        private SwipeRefreshLayout _swipeRefreshLayout;
        private View _emptyPlaceholder;

        public override bool UserVisibleHint
        {
            get => base.UserVisibleHint;
            set
            {
                base.UserVisibleHint = value;

                if (value)
                {
                    ViewModel.OnAppearing();
                }
                else
                {
                    ViewModel.OnDisappearing();
                }
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.fragment_notifications, container, false);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            _navigationBarView =
                View.FindViewById<NavigationBarView>(Resource.Id.fragment_notifications_navigation_bar);
            _navigationBarView.SetCenterImage(StyleHelper.Style.NavigationLogoIcon, null);
            _navigationBarView.SetSettingsButtonAsync(ViewModel.UserName, ViewModel.UserPhotoUrl, ViewModel.RightCommand,
                StyleHelper.Style.ProfileAvatarStyles);

            _recyclerView = View.FindViewById<RecyclerView>(Resource.Id.fragment_notifications_list_view);
            _recyclerView.SetLayoutManager(new LinearLayoutManager(View.Context));
            _adapter = new ObservableRecyclerGroupViewAdapter<string, NotificationViewModel>(
                ViewModel.Notifications,
                (viewGroup, index) =>
                {
                    var cellView = LayoutInflater.From(viewGroup.Context)
                        .Inflate(Resource.Layout.fragment_notifications_cell, viewGroup, false);
                    return new NotificationsViewHolder(cellView);
                },
                (viewGroup, index) =>
                {
                    var cellView = LayoutInflater.From(viewGroup.Context)
                        .Inflate(Resource.Layout.activity_notifications_settings_header, viewGroup, false);
                    return new NotificationsHeaderViewHolder(cellView);
                },
                (viewHolder, index, item, isLast) =>
                {
                    var notificationsViewHolder = (NotificationsViewHolder)viewHolder;
                    notificationsViewHolder.Bind(item, isLast);
                },
                (viewHolder, index, item) =>
                {
                    var headerViewHolder = (NotificationsHeaderViewHolder)viewHolder;
                    headerViewHolder.Bind(item);
                });
            _recyclerView.SetAdapter(_adapter);
            _adapter.SetCommand(nameof(_adapter.LastItemRequested), new RelayCommand(() => { ViewModel.LoadNextPage(); }));

            _swipeRefreshLayout =
                View.FindViewById<SwipeRefreshLayout>(Resource.Id.fragment_notifications_swiperefresh);
            _swipeRefreshLayout.SetCommand(nameof(_swipeRefreshLayout.Refresh), ViewModel.RefreshCommand);

            _busyOverlayView = View.FindViewById<BusyOverlayView>(Resource.Id.fragment_notifications_busy_view);
            _busyOverlayView.SetOverlayBackgroundResource(Resource.Color.notifications_content_color);

            _emptyPlaceholder = View.FindViewById<View>(Resource.Id.fragment_notifications_empty_placeholder);
            _emptyPlaceholder.Visibility = ViewStates.Gone;

            var emptyTextView = View.FindViewById<TextView>(Resource.Id.fragment_notifications_no_data_text);
            emptyTextView.Text = ViewModel.NotificationsLocalizedStrings.NoNotifications;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            _adapter.Dispose();
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
            {
                _busyOverlayView.Visibility = BoolToViewStateConverter.ConvertGone(ViewModel.IsBusy);
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsRefreshing).WhenSourceChanges(() =>
            {
                if (ViewModel.IsRefreshing == false && _swipeRefreshLayout.Refreshing)
                {
                    _swipeRefreshLayout.Refreshing = false;
                }
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.HasData).WhenSourceChanges(() =>
            {
                _emptyPlaceholder.Visibility = BoolToViewStateConverter.ConvertGone(!ViewModel.HasData);
            }));
        }
    }
}