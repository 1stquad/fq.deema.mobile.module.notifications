﻿using System.Linq;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.WhiteLabel.Droid.Navigation;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid.Views
{
    public class NotificationsRootFrameFragment : RootFrameFragmentBase<NotificationsRootFrameViewModel>
    {
        public override bool UserVisibleHint
        {
            get => base.UserVisibleHint;
            set
            {
                base.UserVisibleHint = value;
                ChangeUserVisibleHint(value);
            }
        }

        private void ChangeUserVisibleHint(bool visibile)
        {
            // HACK YP: transfer visibility state to internal fragment
            var notificationsFragment = FragmentManager.Fragments.FirstOrDefault(x => x is NotificationsFragment);
            if (notificationsFragment != null)
            {
                notificationsFragment.UserVisibleHint = visibile;
            }
        }
    }
}