using FQ.DEEMA.Mobile.Module.Notifications.Messages;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.Droid.Services;
using Softeq.XToolkit.WhiteLabel.Messenger;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid.Services
{
    public class NotificationsService : INotificationsService
    {
        private readonly DroidToastService _droidToastService;
        private readonly INotificationsLocalizedStrings _notificationsLocalizedStrings;

        public NotificationsService(
            DroidToastService droidToastService,
            INotificationsLocalizedStrings notificationsLocalizedStrings)
        {
            _droidToastService = droidToastService;
            _notificationsLocalizedStrings = notificationsLocalizedStrings;
        }

        public void HandleNotification(NewsNotification notification)
        {
            ShowToastNotification(notification.DisplayText, new RelayCommand(() =>
            {
                Messenger.Default.Send(new OpenArticleMessage(notification.ArticleId));
            }));
        }

        public void HandleNotification(ChatNotification notification)
        {
            ShowToastNotification(notification.DisplayText, new RelayCommand(() =>
            {
                Messenger.Default.Send(new OpenChatMessage(notification.ChannelId));
            }));
        }

        private void ShowToastNotification(string label, RelayCommand openCommand)
        {
            var model = new ToastModel
            {
                CommandAction = new CommandAction
                {
                    Command = openCommand,
                    Title = _notificationsLocalizedStrings.Open
                },
                Label = label
            };

            _droidToastService.Enqueue(model);
        }
    }
}