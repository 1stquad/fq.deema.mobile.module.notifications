﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.WhiteLabel;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid.ViewHolders
{
    internal class NotificationsHeaderViewHolder : RecyclerView.ViewHolder
    {
        private TextView _textView;
        private readonly INotificationsLocalizedStrings _notificationsLocalizedStrings;

        public NotificationsHeaderViewHolder(View itemView) : base(itemView)
        {
            _textView = itemView.FindViewById<TextView>(Resource.Id.activity_notifications_settings_header_text_view);
            _notificationsLocalizedStrings = Dependencies.IocContainer.Resolve<INotificationsLocalizedStrings>();
        }

        public void Bind(string value)
        {
            _textView.Text = value;
        }
    }
}
