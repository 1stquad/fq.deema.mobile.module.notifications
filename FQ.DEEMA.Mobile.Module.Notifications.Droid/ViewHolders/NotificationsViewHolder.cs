﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.Common.Droid.Helpers;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid.ViewHolders
{
    internal class NotificationsViewHolder : RecyclerView.ViewHolder
    {
        private readonly View _cellView;
        private readonly TextView _dateTextView;
        private readonly ShortDateTimeToStringConverter _dateTimeToStringConverter;
        private readonly View _separator;
        private readonly View _altSeparator;
        private readonly TextView _textView;

        private WeakReferenceEx<NotificationViewModel> _viewModelRef;

        public NotificationsViewHolder(View itemView) : base(itemView)
        {
            _textView = itemView.FindViewById<TextView>(Resource.Id.fragment_notifications_cell_text);
            _dateTextView = itemView.FindViewById<TextView>(Resource.Id.fragment_notifications_cell_date);
            _dateTimeToStringConverter =
                new ShortDateTimeToStringConverter(LocaleHelper.Is24HourFormat(itemView.Context));
            _separator = itemView.FindViewById<View>(Resource.Id.fragment_notifications_cell_view_separator);
            _altSeparator = itemView.FindViewById<View>(Resource.Id.fragment_notifications_cell_view_alt_separator);
            _cellView = itemView.FindViewById<View>(Resource.Id.fragment_notifications_cell_view);
            _cellView.Click += OnCellClick;
        }

        public void Bind(NotificationViewModel item, bool isLast)
        {
            _separator.Visibility = BoolToViewStateConverter.ConvertGone(!isLast);
            _altSeparator.Visibility = BoolToViewStateConverter.ConvertGone(isLast);
            _textView.Text = item.Title;
            _dateTextView.Text = _dateTimeToStringConverter.ConvertValue(item.Notification.Created);
            _viewModelRef = WeakReferenceEx.Create(item);
        }

        private void OnCellClick(object sender, EventArgs e)
        {
            _viewModelRef.Target.OpenArticle();
        }
    }
}
