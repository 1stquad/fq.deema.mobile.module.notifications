﻿using Autofac;
using FQ.DEEMA.Mobile.Module.Notifications.Droid.Services;
using FQ.DEEMA.Mobile.Module.Notifications.Services;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid
{
    public static class BootstrapperDroid
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            Bootstrapper.Configure(containerBuilder);

            containerBuilder.RegisterType<NotificationsService>().As<INotificationsService>().InstancePerDependency();
        }
    }
}