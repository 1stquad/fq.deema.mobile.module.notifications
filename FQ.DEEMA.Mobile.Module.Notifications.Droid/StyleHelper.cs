﻿using System;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;

namespace FQ.DEEMA.Mobile.Module.Notifications.Droid
{
    internal static class StyleHelper
    {
        private static readonly Lazy<INotificationsDroidStyle> StyleLazy =
            Dependencies.IocContainer.LazyResolve<INotificationsDroidStyle>();

        public static INotificationsDroidStyle Style => StyleLazy.Value;
    }

    public interface INotificationsDroidStyle
    {
        int NavigationBarBackButtonIcon { get; }
        int NavigationLogoIcon { get; }

        AvatarPlaceholderDrawable.AvatarStyles ProfileAvatarStyles { get; }
    }
}
