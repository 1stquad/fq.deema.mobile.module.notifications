﻿using System.Windows.Input;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;

namespace FQ.DEEMA.Mobile.Module.Notifications.ViewModels
{
    public class NotificationsRootFrameViewModel : RootFrameNavigationViewModelBase
    {
        public NotificationsRootFrameViewModel(IFrameNavigationService frameNavigationService)
            : base(frameNavigationService)
        {
        }

        public ICommand RightCommand { get; set; }

        public override void NavigateToFirstPage()
        {
            FrameNavigationService.NavigateToViewModel<NotificationsViewModel, ICommand[]>(
                new[] {RightCommand});
        }
    }
}