﻿using System.Linq;
using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.Common.Collections;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.Notifications.ViewModels
{
    public class NotificationsSettingsViewModel : ViewModelBase
    {
        private readonly INotificationsLocalizedStrings _notificationsLocalizedStrings;
        private readonly IPageNavigationService _pageNavigationService;
        private readonly INotificationsManager _notificationsManager;

        public NotificationsSettingsViewModel(
            INotificationsLocalizedStrings notificationsLocalizedStrings,
            IPageNavigationService pageNavigationService,
            INotificationsManager notificationsManager)
        {
            _notificationsLocalizedStrings = notificationsLocalizedStrings;
            _pageNavigationService = pageNavigationService;
            _notificationsManager = notificationsManager;
        }

        public ObservableKeyGroupsCollection<string, NotificationSettingViewModel> Settings { get; }
            = new ObservableKeyGroupsCollection<string, NotificationSettingViewModel>();

        public string Title => _notificationsLocalizedStrings.Notifications;

        public ICommand BackCommand { get; private set; }

        public override void OnInitialize()
        {
            base.OnInitialize();

            BackCommand = new RelayCommand(_pageNavigationService.GoBack);

            IsBusy = true;

            _notificationsManager.GetNotificationSettingsAsync().ContinueOnUiThread(t =>
            {
                IsBusy = false;

                if (t == null)
                {
                    return;
                }

                var viewModels = t.Select(x => new NotificationSettingViewModel(
                        x,
                        new NotificationSettingTypeToStringConverter(_notificationsLocalizedStrings),
                        _notificationsManager))
                    .ToArray();

                Settings.ReplaceRangeGroup(viewModels, x => _notificationsLocalizedStrings.NotificationType);
            });
        }
    }
}