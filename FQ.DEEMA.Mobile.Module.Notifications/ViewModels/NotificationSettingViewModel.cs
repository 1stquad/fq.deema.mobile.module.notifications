﻿using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.Notifications.ViewModels
{
    public class NotificationSettingViewModel : ObservableObject
    {
        private readonly NotificationSettingTypeToStringConverter _notificationSettingTypeToStringConverter;
        private readonly INotificationsManager _notificationsManager;

        public NotificationSettingViewModel(
            NotificationSetting model,
            NotificationSettingTypeToStringConverter notificationSettingTypeToStringConverter,
            INotificationsManager notificationsManager)
        {
            _notificationSettingTypeToStringConverter = notificationSettingTypeToStringConverter;
            _notificationsManager = notificationsManager;
            NotificationSetting = model;
        }

        public NotificationSetting NotificationSetting { get; }

        public string DisplayName =>
            _notificationSettingTypeToStringConverter.ConvertValue(NotificationSetting.NotificationSettingType);

        public bool IsEnabled
        {
            get => NotificationSetting.IsEnabled;
            set
            {
                if (NotificationSetting.IsEnabled == value)
                {
                    return;
                }

                NotificationSetting.IsEnabled = value;
                _notificationsManager.SetNotificationSettingAsync(NotificationSetting.NotificationSettingType,
                    value);
            }
        }
    }
}