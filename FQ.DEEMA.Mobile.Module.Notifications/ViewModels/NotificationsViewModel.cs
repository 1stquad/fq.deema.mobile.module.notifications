﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.Notifications.Messages;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using FQ.DEEMA.Mobile.Module.Notifications.Services.Remote;
using Softeq.XToolkit.Auth;
using Softeq.XToolkit.Common.Collections;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.Common.Models;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Messenger;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.Notifications.ViewModels
{
    public class NotificationsViewModel : ViewModelWithPagingBase<NewsNotification>, IViewModelParameter<ICommand[]>
    {
        private readonly INotificationsManager _notificationsManager;
        private readonly IAccountService _accountService;
        private readonly IRemoteNotificationsService _remoteNotificationsService;

        private bool _isRefreshing;
        private bool _hasData = true;
        private DateTime _startDate;

        public NotificationsViewModel(
            IRemoteNotificationsService remoteNotificationsService,
            INotificationsLocalizedStrings notificationsLocalizedStrings,
            INotificationsManager notificationsManager,
            IAccountService accountService)
        {
            _remoteNotificationsService = remoteNotificationsService;
            NotificationsLocalizedStrings = notificationsLocalizedStrings;
            _notificationsManager = notificationsManager;
            _accountService = accountService;

            RefreshCommand = new RelayCommand(RefreshData);
        }

        public INotificationsLocalizedStrings NotificationsLocalizedStrings { get; }

        public ICommand RefreshCommand { get; }

        public ICommand RightCommand { get; private set; }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => Set(ref _isRefreshing, value);
        }

        public bool HasData
        {
            get => _hasData;
            set => Set(ref _hasData, value);
        }

        public ObservableKeyGroupsCollection<string, NotificationViewModel> Notifications { get; }
            = new ObservableKeyGroupsCollection<string, NotificationViewModel>();

        public ICommand[] Parameter
        {
            get => null;
            set
            {
                RightCommand = value[0];
            }
        }

        public string UserPhotoUrl => _accountService.UserPhotoUrl;

        public string UserName => _accountService.UserName;

        public override void OnAppearing()
        {
            base.OnAppearing();

            Messenger.Default.Register<UnreadNotificationsMessage>(this, OnUnreadNotificationsMessage);

            if (Notifications.Count == 0)
            {
                LoadData();
            }
            else
            {
                RefreshData();
            }
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            Messenger.Default.Unregister<UnreadNotificationsMessage>(this, OnUnreadNotificationsMessage);
        }

        public void LoadNextPage()
        {
            LoadNextPageAsync(false, CancellationToken.None).SafeTaskWrapper();
        }

        protected override Task<bool> AddPage(IList<NewsNotification> data, bool shouldReset)
        {
            if (data == null)
            {
                return Task.FromResult(false);
            }

            var tcs = new TaskCompletionSource<bool>();
            Execute.BeginOnUIThread(() =>
            {
                if (shouldReset)
                {
                    Notifications.ReplaceRangeGroup(CreateViewModels(data), x => ConvertReadToString(x.Notification.IsRead));
                    HasData = Notifications.Count > 0;

                    tcs.SetResult(true);
                }
                else
                {
                    if (data.Count > 0)
                    {
                        Notifications.AddRangeToGroups(CreateViewModels(data), x => ConvertReadToString(x.Notification.IsRead));
                    }
                    HasData = Notifications.Count > 0;

                    tcs.SetResult(true);
                }
            });
            return tcs.Task;
        }

        protected override Task<PagingModel<NewsNotification>> GetItems(int page, int perPage)
        {
            return _remoteNotificationsService.GetNotificationsAsync(page, perPage, _startDate);
        }

        private void LoadData()
        {
            _startDate = DateTime.Now;
            IsBusy = true;
            LoadFirstPageAsync(CancellationToken.None).ContinueOnUiThread(() =>
            {
                IsBusy = false;

                _notificationsManager.MarkAllNotificationsAsRead();

                _notificationsManager.RefreshOnBackgroundAsync();
            });
        }

        private void RefreshData()
        {
            RefreshData(true);
        }

        private void RefreshData(bool updateWithBackgroundLoad)
        {
            _startDate = DateTime.Now;
            IsRefreshing = true;
            LoadFirstPageAsync(CancellationToken.None).ContinueOnUiThread(() =>
            {
                IsRefreshing = false;

                if (updateWithBackgroundLoad)
                {
                    _notificationsManager.RefreshOnBackgroundAsync();
                }
            });
        }

        private IList<NotificationViewModel> CreateViewModels(IList<NewsNotification> data)
        {
            return data.Select(x => new NotificationViewModel(x, NotificationsLocalizedStrings)).ToList();
        }

        private void OnUnreadNotificationsMessage(UnreadNotificationsMessage message)
        {
            if (message.Count > 0 && !IsBusy && !IsRefreshing)
            {
                RefreshData(false);
            }
        }

        private string ConvertReadToString(bool value)
        {
            return value ? NotificationsLocalizedStrings.Viewed : NotificationsLocalizedStrings.New;
        }
    }
}