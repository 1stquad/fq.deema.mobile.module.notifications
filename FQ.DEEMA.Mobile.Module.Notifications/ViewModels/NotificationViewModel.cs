﻿using FQ.DEEMA.Mobile.Module.Notifications.Messages;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.WhiteLabel.Messenger;

namespace FQ.DEEMA.Mobile.Module.Notifications.ViewModels
{
    public class NotificationViewModel
    {
        private readonly NotificationTypeToStringConverter _notificationTypeToStringConverter;
        private readonly INotificationsLocalizedStrings _notificationsLocalizedStrings;

        public NotificationViewModel(
            NewsNotification notification,
            INotificationsLocalizedStrings notificationsLocalizedStrings)
        {
            Notification = notification;
            _notificationTypeToStringConverter = new NotificationTypeToStringConverter(notificationsLocalizedStrings);
            _notificationsLocalizedStrings = notificationsLocalizedStrings;
        }

        public string Title
        {
            get
            {
                return _notificationTypeToStringConverter.ConvertValue((Notification.Type, Notification.NewsHeader));
            }
        }

        public NewsNotification Notification { get; }

        public void OpenArticle()
        {
            Messenger.Default.Send(new OpenArticleMessage(Notification.ArticleId));
        }
    }
}