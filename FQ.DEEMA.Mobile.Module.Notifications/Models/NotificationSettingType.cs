﻿namespace FQ.DEEMA.Mobile.Module.Notifications.Models
{
    public enum NotificationSettingType
    {
        Unknown = 0,
        CommentLiked = 1,
        ArticleLiked = 2,
        CommentMention = 3,
        ArticleMention = 4,
        ArticleAdded = 5,
        ChatNotifications = 6
    }
}