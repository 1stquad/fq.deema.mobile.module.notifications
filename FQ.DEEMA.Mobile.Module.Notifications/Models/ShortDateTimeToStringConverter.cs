using System;
using System.Globalization;
using Softeq.XToolkit.Common.Extensions;

namespace FQ.DEEMA.Mobile.Module.Notifications.Models
{
    public class ShortDateTimeToStringConverter
    {
        private readonly bool _is24HFormat;

        public ShortDateTimeToStringConverter(bool is24HFormat)
        {
            _is24HFormat = is24HFormat;
        }

        public string ConvertValue(DateTime value, object parameter = null, string language = null)
        {
            var format = value.IsToday() ? _is24HFormat ? "HH:mm" : "hh:mm tt" : "dd MMM";
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        public DateTime ConvertValueBack(string value, object parameter = null, string language = null)
        {
            throw new NotImplementedException();
        }
    }
}