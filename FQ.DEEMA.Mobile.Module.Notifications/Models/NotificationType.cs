﻿namespace FQ.DEEMA.Mobile.Module.Notifications.Models
{
    public enum NotificationType
    {
        Unknown = 0,
        NewsPosted = 1,
        CommentLiked = 2,
        ArticleLiked = 3,
        CommentMentioned = 4,
        ArticleMentioned = 5,
        CommentReplied = 6,
        ChatMessage = 7
    }
}
