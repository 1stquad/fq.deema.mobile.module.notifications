﻿using System;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.Common.Interfaces;

namespace FQ.DEEMA.Mobile.Module.Notifications.Models
{
    public class NotificationSettingTypeToStringConverter : IConverter<string, NotificationSettingType>
    {
        private readonly INotificationsLocalizedStrings _notificationsLocalizedStrings;

        public NotificationSettingTypeToStringConverter(INotificationsLocalizedStrings notificationsLocalizedStrings)
        {
            _notificationsLocalizedStrings = notificationsLocalizedStrings;
        }

        public string ConvertValue(NotificationSettingType value, object parameter = null, string language = null)
        {
            switch (value)
            {
                case NotificationSettingType.ArticleAdded:
                    return _notificationsLocalizedStrings.ArticleAddedKey;
                case NotificationSettingType.ArticleLiked:
                    return _notificationsLocalizedStrings.ArticleLikedKey;
                case NotificationSettingType.ArticleMention:
                    return _notificationsLocalizedStrings.ArticleMentionKey;
                case NotificationSettingType.CommentLiked:
                    return _notificationsLocalizedStrings.CommentLikedKey;
                case NotificationSettingType.CommentMention:
                    return _notificationsLocalizedStrings.CommentMentionKey;
                case NotificationSettingType.ChatNotifications:
                    return _notificationsLocalizedStrings.ChatNotificationsKey;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public NotificationSettingType ConvertValueBack(string value, object parameter = null, string language = null)
        {
            throw new NotImplementedException();
        }
    }
}