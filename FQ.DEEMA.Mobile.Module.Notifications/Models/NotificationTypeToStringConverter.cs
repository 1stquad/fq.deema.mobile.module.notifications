﻿using System;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.Common.Interfaces;

namespace FQ.DEEMA.Mobile.Module.Notifications.Models
{
    public class NotificationTypeToStringConverter : IConverter<string, (NotificationType, string)>
    {
        private readonly INotificationsLocalizedStrings _notificationsLocalizedStrings;

        public NotificationTypeToStringConverter(INotificationsLocalizedStrings notificationsLocalizedStrings)
        {
            _notificationsLocalizedStrings = notificationsLocalizedStrings;
        }

        public string ConvertValue((NotificationType, string) value, object parameter = null, string language = null)
        {
            switch (value.Item1)
            {
                case NotificationType.NewsPosted:
                    return string.Format(_notificationsLocalizedStrings.NewsPostedFormat, value.Item2);
                case NotificationType.CommentLiked:
                    return string.Format(_notificationsLocalizedStrings.CommentLikedFormat, value.Item2);
                case NotificationType.ArticleLiked:
                    return string.Format(_notificationsLocalizedStrings.ArticleLikedFormat, value.Item2);
                case NotificationType.CommentMentioned:
                    return string.Format(_notificationsLocalizedStrings.InCommentMentionedFormat, value.Item2);
                case NotificationType.ArticleMentioned:
                    return string.Format(_notificationsLocalizedStrings.InArticleMentionedFormat, value.Item2);
                case NotificationType.CommentReplied:
                    return string.Format(_notificationsLocalizedStrings.CommentRepliedFormat, value.Item2);
                case NotificationType.ChatMessage:
                    return string.Format(_notificationsLocalizedStrings.NewChatMessage);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public (NotificationType, string) ConvertValueBack(string value, object parameter = null, string language = null)
        {
            throw new NotImplementedException();
        }
    }
}