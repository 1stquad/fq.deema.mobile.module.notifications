﻿namespace FQ.DEEMA.Mobile.Module.Notifications.Models
{
    public class NotificationSetting
    {
        public NotificationSettingType NotificationSettingType { get; set; }
        public bool IsEnabled { get; set; }
    }
}