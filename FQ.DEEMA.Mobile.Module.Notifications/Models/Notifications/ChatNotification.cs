﻿namespace FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications
{
    public class ChatNotification : NotificationBase
    {
        public string ChannelId { get; set; }
    }
}
