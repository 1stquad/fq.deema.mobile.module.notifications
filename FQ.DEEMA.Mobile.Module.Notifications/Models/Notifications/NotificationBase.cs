﻿namespace FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications
{
    public abstract class NotificationBase
    {
        public NotificationType Type { get; set; }
        public string DisplayText { get; set; }
    }
}
