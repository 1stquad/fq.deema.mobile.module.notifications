﻿using System;

namespace FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications
{
    public class NewsNotification : NotificationBase
    {
        public string ArticleId { get; set; }
        public string Id { get; set; }
        public DateTime Created { get; set; }
        public string NewsHeader { get; set; }
        public bool IsRead { get; set; }
    }
}
