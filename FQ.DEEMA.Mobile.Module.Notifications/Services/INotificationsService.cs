﻿using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services
{
    public interface INotificationsService
    {
        void HandleNotification(NewsNotification notification);

        void HandleNotification(ChatNotification notification);
    }
}