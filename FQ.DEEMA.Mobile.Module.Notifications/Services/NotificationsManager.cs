using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.Notifications.Handlers;
using FQ.DEEMA.Mobile.Module.Notifications.Messages;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using FQ.DEEMA.Mobile.Module.Notifications.Services.Remote;
using FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Dtos;
using FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Requests;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.RemoteData;
using Softeq.XToolkit.RemoteData.HttpClient;
using Softeq.XToolkit.WhiteLabel.Messenger;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services
{
    public class NotificationsManager : INotificationsManager
    {
        private const string NotificationSettingsCacheKey = "notifications_settings";
        private readonly NotificationTypeToStringConverter _converter;
        private readonly ILocalCache _localCache;
        private readonly ILogger _logger;
        private readonly IRemoteNotificationsService _remoteNotificationsService;
        private readonly INotificationsService _notificationsService;
        private readonly IEnumerable<INotificationProcessor> _notificationProcessors;
        private readonly IRestHttpClient _restHttpClient;

        public NotificationsManager(
            ILogManager logManager,
            INotificationsLocalizedStrings notificationsLocalizedStrings,
            INotificationsService notificationsService,
            IRestHttpClient restHttpClient,
            ILocalCache localCache,
            IRemoteNotificationsService remoteNotificationsService,
            IEnumerable<INotificationProcessor> notificationProcessors)
        {
            _restHttpClient = restHttpClient;
            _logger = logManager.GetLogger<NotificationsManager>();
            _localCache = localCache;
            _remoteNotificationsService = remoteNotificationsService;
            _notificationsService = notificationsService;
            _notificationProcessors = notificationProcessors;
            _converter = new NotificationTypeToStringConverter(notificationsLocalizedStrings);
        }

        public void HandleAsInAppNotification(NotificationType notificationType, string payload)
        {
            _notificationProcessors
                .Where(x => x.CanHandle(notificationType))
                .Apply(x =>
                {
                    var notification = x.TryParse(payload);

                    if (notification == null)
                    {
                        return;
                    }

                    notification.Type = notificationType;
                    notification.DisplayText = _converter.ConvertValue((notificationType, x.GetDisplayText(notification)));

                    x.HandleAsInApp(notification);
                });

            CheckForNewNotificationsAsync().SafeTaskWrapper();
        }

        public void HandleAsRemoteNotification(NotificationType notificationType, string payload)
        {
            _notificationProcessors
                .Where(x => x.CanHandle(notificationType))
                .Apply(x =>
                {
                    var notification = x.TryParse(payload);

                    if (notification == null)
                    {
                        return;
                    }

                    x.HandleAsRemote(notification);
                });
        }

        public void DisplayCustomInAppNewsNotification(NotificationType notificationType, NewsNotification notification)
        {
            if (string.IsNullOrEmpty(notification.ArticleId) ||
                string.IsNullOrEmpty(notification.NewsHeader))
            {
                var message = $"{nameof(NewsNotification)}: {nameof(notification.ArticleId)} and {nameof(notification.NewsHeader)} must be declared!";
                _logger.Error(new ArgumentException(message));
                return;
            }

            notification.DisplayText = _converter.ConvertValue((notificationType, notification.NewsHeader));

            _notificationsService.HandleNotification(notification);
        }

        public void RefreshOnBackgroundAsync()
        {
            Task.WhenAll(CheckForNewNotificationsAsync(), UpdateCacheAsync());
        }

        public Task<IList<NotificationSetting>> GetNotificationSettingsAsync()
        {
            return _localCache.Get<IList<NotificationSetting>>(NotificationSettingsCacheKey);
        }

        public async void SetNotificationSettingAsync(
            NotificationSettingType notificationSettingNotificationSettingType, bool value)
        {
            _remoteNotificationsService.SetNotificationSettingAsync(notificationSettingNotificationSettingType, value);

            var cachedSettings =
                await _localCache.Get<IList<NotificationSetting>>(NotificationSettingsCacheKey);

            var selectedSetting = cachedSettings?
                .FirstOrDefault(x => x.NotificationSettingType == notificationSettingNotificationSettingType);
            if (selectedSetting == null)
            {
                return;
            }

            selectedSetting.IsEnabled = value;
            await _localCache.Add(NotificationSettingsCacheKey, DateTimeOffset.UtcNow, cachedSettings)
                .ConfigureAwait(false);
        }

        public void MarkAllNotificationsAsRead()
        {
            _remoteNotificationsService.MarkAllNotificationsAsReadAsync().ContinueWith(t =>
            {
                Messenger.Default.Send(new UnreadNotificationsMessage(count: 0));
            });
        }

        private async Task CheckForNewNotificationsAsync()
        {
            var request = new GetNotificationsCountRequest();
            var dto = await _restHttpClient.TrySendAndDeserializeAsync<NotificationsCountDto>(request, _logger)
                .ConfigureAwait(false);

            if (dto == null)
            {
                return;
            }

            Messenger.Default.Send(new UnreadNotificationsMessage(dto.UnreadNotificationsNumber));
        }

        private async Task UpdateCacheAsync()
        {
            var notificationSettings =
                await _remoteNotificationsService.GetNotificationSettingsAsync().ConfigureAwait(false);
            if (notificationSettings == null)
            {
                return;
            }

            await _localCache.Add(NotificationSettingsCacheKey, DateTimeOffset.UtcNow, notificationSettings)
                .ConfigureAwait(false);
        }
    }
}