﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Dtos;
using FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Requests;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.Common.Models;
using Softeq.XToolkit.RemoteData;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote
{
    public class RemoteNotificationsService : IRemoteNotificationsService
    {
        private readonly IJsonSerializer _jsonSerializer;
        private readonly ILogger _logger;
        private readonly IRestHttpClient _restHttpClient;

        public RemoteNotificationsService(
            IJsonSerializer jsonSerializer,
            IRestHttpClient restHttpClient,
            ILogManager logManager)
        {
            _jsonSerializer = jsonSerializer;
            _restHttpClient = restHttpClient;
            _logger = logManager.GetLogger<RemoteNotificationsService>();
        }

        public Task<PagingModel<NewsNotification>> GetNotificationsAsync(int page, int pageSize, DateTime dateTime)
        {
            var dto = new GetNotificationsRequestBodyDto
            {
                Page = page,
                PageSize = pageSize,
                Sort = new SortDto { Order = 1, PropertyName = "created" },
                MaxCreationDate = dateTime.ToUniversalTime()
            };

            var request = new GetNotificationsRequest(_jsonSerializer, dto);

            return _restHttpClient.GetPagingModelAsync<NewsNotification, NotificationDto>(
                request, _logger, x =>
                {
                    return new NewsNotification
                    {
                        ArticleId = x.ArticleId,
                        Created = x.Created.ToLocalTime(),
                        Id = x.Id,
                        IsRead = x.IsRead,
                        Type = (NotificationType)x.Type,
                        NewsHeader = x.ArticleTitle
                    };
                });
        }

        public Task<IList<NotificationSetting>> GetNotificationSettingsAsync()
        {
            var request = new GetNotificationsSettingsRequest();
            return _restHttpClient.GetModelAsync<IList<NotificationSetting>, NotificationsDto>(request, _logger, dto =>
            {
                return new List<NotificationSetting>
                {
                    new NotificationSetting
                    {
                        NotificationSettingType = NotificationSettingType.CommentLiked,
                        IsEnabled = dto.CommentLiked
                    },
                    new NotificationSetting
                    {
                        NotificationSettingType = NotificationSettingType.ArticleLiked,
                        IsEnabled = dto.ArticleLiked
                    },
                    new NotificationSetting
                    {
                        NotificationSettingType = NotificationSettingType.CommentMention,
                        IsEnabled = dto.CommentMention
                    },
                    //new NotificationSetting
                    //{
                    //    NotificationSettingType = NotificationSettingType.ArticleMention,
                    //    IsEnabled = dto.ArticleMention
                    //},
                    new NotificationSetting
                    {
                        NotificationSettingType = NotificationSettingType.ArticleAdded,
                        IsEnabled = dto.ArticleAdded
                    }
                };
            });
        }

        public void SetNotificationSettingAsync(NotificationSettingType notificationSettingType, bool value)
        {
            var request = new PutNotificationSettingRequest(notificationSettingType, value);
            _restHttpClient.TrySendAsync(request, _logger).SafeTaskWrapper();
        }

        public async Task MarkAllNotificationsAsReadAsync()
        {
            var request = new PostNotificationsMarkAsRead();
            await _restHttpClient.TrySendAsync(request, _logger).ConfigureAwait(false);
        }
    }
}