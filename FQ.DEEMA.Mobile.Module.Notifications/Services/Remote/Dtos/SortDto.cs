namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Dtos
{
    internal class SortDto
    {
        public string PropertyName { get; set; }

        public int Order { get; set; }
    }
}