namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Dtos
{
    internal class NotificationsCountDto
    {
        public int UnreadNotificationsNumber { get; set; }
    }
}