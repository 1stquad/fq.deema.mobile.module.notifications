﻿namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Dtos
{
    internal class NotificationsDto
    {
        public bool CommentLiked { get; set; }
        public bool ArticleLiked { get; set; }
        public bool CommentMention { get; set; }
        public bool ArticleMention { get; set; }
        public bool ArticleAdded { get; set; }
        public bool ChatNotifications { get; set; }
    }
}