﻿using System;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Dtos
{
    internal class GetNotificationsRequestBodyDto
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public DateTime MaxCreationDate { get; set; }

        public SortDto Sort { get; set; }
    }
}