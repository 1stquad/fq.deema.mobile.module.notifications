﻿using System;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Dtos
{
    internal class NotificationDto
    {
        public string ArticleId { get; set; }
        public string Id { get; set; }
        public DateTime Created { get; set; }
        public int Type { get; set; }
        public bool IsRead { get; set; }
        public string ArticleTitle { get; set; }
    }
}