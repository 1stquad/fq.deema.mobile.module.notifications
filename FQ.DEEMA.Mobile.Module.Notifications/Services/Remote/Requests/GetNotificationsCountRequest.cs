using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Requests
{
    internal class GetNotificationsCountRequest : BaseRestRequest
    {
        public override string EndpointUrl => "/me";
    }
}