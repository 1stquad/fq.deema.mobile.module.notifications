﻿using System.Net.Http;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Requests
{
    internal class PostNotificationsMarkAsRead : BaseRestRequest
    {
        public override HttpMethod Method => HttpMethod.Post;

        public override string EndpointUrl => $"/notifications/mark-as-read";
    }
}
