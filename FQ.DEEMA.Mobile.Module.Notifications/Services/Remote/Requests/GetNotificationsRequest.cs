﻿using FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Dtos;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Requests
{
    internal class GetNotificationsRequest : BasePostRestRequest<GetNotificationsRequestBodyDto>
    {
        public GetNotificationsRequest(IJsonSerializer jsonSerializer, GetNotificationsRequestBodyDto dto)
            : base(jsonSerializer, dto)
        {
        }

        public override string EndpointUrl => "/notifications";
    }
}