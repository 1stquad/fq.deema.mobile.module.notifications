﻿using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Requests
{
    internal class GetNotificationsSettingsRequest : BaseRestRequest
    {
        public override string EndpointUrl => "/me/settings/notification";
    }
}