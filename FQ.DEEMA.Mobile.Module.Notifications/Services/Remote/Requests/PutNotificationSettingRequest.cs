﻿using System.Net.Http;
using System.Text;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using Softeq.XToolkit.RemoteData;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote.Requests
{
    internal class PutNotificationSettingRequest : BaseRestRequest
    {
        private readonly NotificationSettingType _notificationSettingType;
        private readonly bool _value;

        public PutNotificationSettingRequest(
            NotificationSettingType notificationSettingType,
            bool value)
        {
            _notificationSettingType = notificationSettingType;
            _value = value;
        }

        public override HttpMethod Method => HttpMethod.Put;

        public override string EndpointUrl => $"/me/settings/notification/{_notificationSettingType}";

        public override HttpContent GetContent()
        {
            return new StringContent(_value ? "1" : "0", Encoding.UTF8, HttpConsts.ApplicationJsonHeaderValue);
        }
    }
}