﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using Softeq.XToolkit.Common.Models;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services.Remote
{
    public interface IRemoteNotificationsService
    {
        Task<IList<NotificationSetting>> GetNotificationSettingsAsync();
        Task<PagingModel<NewsNotification>> GetNotificationsAsync(int page, int pageSize, DateTime dateTime);
        void SetNotificationSettingAsync(NotificationSettingType notificationSettingType, bool value);
        Task MarkAllNotificationsAsReadAsync();
    }
}