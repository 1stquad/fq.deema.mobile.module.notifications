﻿namespace FQ.DEEMA.Mobile.Module.Notifications.Services
{
    public interface INotificationsLocalizedStrings
    {
        string Notifications { get; }
        string CommentLikedKey { get; }
        string ArticleLikedKey { get; }
        string CommentMentionKey { get; }
        string ArticleMentionKey { get; }
        string ArticleAddedKey { get; }
        string NewsPostedFormat { get; }
        string CommentLikedFormat { get; }
        string ArticleLikedFormat { get; }
        string InCommentMentionedFormat { get; }
        string InArticleMentionedFormat { get; }
        string CommentRepliedFormat { get; }
        string Open { get; }
        string NotificationType { get; }
        string Viewed { get; }
        string New { get; }
        string NoNotifications { get; }
        string ChatNotificationsKey { get; }
        string NewChatMessage { get; }
    }
}