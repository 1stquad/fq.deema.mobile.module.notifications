using System.Collections.Generic;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;

namespace FQ.DEEMA.Mobile.Module.Notifications.Services
{
    public interface INotificationsManager
    {
        void HandleAsInAppNotification(NotificationType notificationType, string payload);
        void HandleAsRemoteNotification(NotificationType notificationType, string payload);
        void RefreshOnBackgroundAsync();

        void DisplayCustomInAppNewsNotification(NotificationType notificationType, NewsNotification notification);

        void SetNotificationSettingAsync(NotificationSettingType notificationSettingNotificationSettingType, bool value);
        Task<IList<NotificationSetting>> GetNotificationSettingsAsync();

        void MarkAllNotificationsAsRead();
    }
}