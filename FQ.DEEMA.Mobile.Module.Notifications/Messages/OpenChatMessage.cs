﻿namespace FQ.DEEMA.Mobile.Module.Notifications.Messages
{
    public class OpenChatMessage
    {
        public OpenChatMessage(string channelId)
        {
            ChannelId = channelId;
        }

        public string ChannelId { get; }
    }
}
