namespace FQ.DEEMA.Mobile.Module.Notifications.Messages
{
    public class UnreadNotificationsMessage
    {
        public UnreadNotificationsMessage(int count)
        {
            Count = count;
        }

        public int Count { get; }
    }
}