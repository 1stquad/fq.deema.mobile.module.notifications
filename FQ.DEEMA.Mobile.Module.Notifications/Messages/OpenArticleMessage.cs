namespace FQ.DEEMA.Mobile.Module.Notifications.Messages
{
    public class OpenArticleMessage
    {
        public OpenArticleMessage(string articleId)
        {
            ArticleId = articleId;
        }

        public string ArticleId { get; }
    }
}