﻿using System;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using Softeq.XToolkit.Common.Interfaces;

namespace FQ.DEEMA.Mobile.Module.Notifications.Handlers
{
    public abstract class NotificationProcessorBase<T> : INotificationProcessor where T : NotificationBase
    {
        private readonly IJsonSerializer _jsonSerializer;
        private readonly ILogger _logger;

        protected NotificationProcessorBase(IJsonSerializer jsonSerializer, ILogManager logManager)
        {
            _jsonSerializer = jsonSerializer;
            _logger = logManager.GetLogger<NotificationProcessorBase<T>>();
        }

        public abstract bool CanHandle(NotificationType notificationType);

        public abstract void HandleAsInApp(NotificationBase notification);

        public abstract void HandleAsRemote(NotificationBase notification);

        public NotificationBase TryParse(string payload)
        {
            return TryParseToNotification(payload);
        }

        public abstract string GetDisplayText(NotificationBase notification);

        private T TryParseToNotification(string payload)
        {
            var notification = default(T);

            try
            {
                notification = _jsonSerializer.Deserialize<T>(payload);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return notification;
        }
    }
}
