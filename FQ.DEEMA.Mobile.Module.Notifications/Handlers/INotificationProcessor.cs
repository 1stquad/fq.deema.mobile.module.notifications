﻿using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;

namespace FQ.DEEMA.Mobile.Module.Notifications.Handlers
{
    public interface INotificationProcessor
    {
        bool CanHandle(NotificationType notificationType);

        void HandleAsInApp(NotificationBase notification);
        void HandleAsRemote(NotificationBase notification);

        NotificationBase TryParse(string payload);

        string GetDisplayText(NotificationBase notification);
    }
}
