﻿using FQ.DEEMA.Mobile.Module.Notifications.Messages;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.WhiteLabel.Messenger;

namespace FQ.DEEMA.Mobile.Module.Notifications.Handlers
{
    public class NewsNotificationProcessor : NotificationProcessorBase<NewsNotification>
    {
        private readonly INotificationsService _notificationsService;

        public NewsNotificationProcessor(
            IJsonSerializer jsonSerializer,
            ILogManager logManager,
            INotificationsService notificationsService)
            : base(jsonSerializer, logManager)
        {
            _notificationsService = notificationsService;
        }

        public override bool CanHandle(NotificationType notificationType)
        {
            switch (notificationType)
            {
                case NotificationType.ArticleLiked:
                case NotificationType.ArticleMentioned:
                case NotificationType.CommentLiked:
                case NotificationType.CommentMentioned:
                case NotificationType.CommentReplied:
                case NotificationType.NewsPosted:
                    return true;
                default:
                    return false;
            }
        }

        public override string GetDisplayText(NotificationBase notification)
        {
            if (notification is NewsNotification newsNotification)
            {
                return newsNotification.NewsHeader;
            }
            return string.Empty;
        }

        public override void HandleAsInApp(NotificationBase notification)
        {
            if (notification is NewsNotification newsNotification)
            {
                _notificationsService.HandleNotification(newsNotification);
            }
        }

        public override void HandleAsRemote(NotificationBase notification)
        {
            if (notification is NewsNotification newsNotification)
            {
                Messenger.Default.Send(new OpenArticleMessage(newsNotification.ArticleId));
            }
        }
    }
}
