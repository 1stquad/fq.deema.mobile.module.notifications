﻿using FQ.DEEMA.Mobile.Module.Notifications.Messages;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.WhiteLabel.Messenger;

namespace FQ.DEEMA.Mobile.Module.Notifications.Handlers
{
    public class ChatNotificationProcessor : NotificationProcessorBase<ChatNotification>
    {
        private readonly INotificationsService _notificationsService;

        public ChatNotificationProcessor(
            IJsonSerializer jsonSerializer,
            ILogManager logManager,
            INotificationsService notificationsService)
            : base(jsonSerializer, logManager)
        {
            _notificationsService = notificationsService;
        }

        public override bool CanHandle(NotificationType notificationType)
        {
            return notificationType == NotificationType.ChatMessage;
        }

        public override string GetDisplayText(NotificationBase notification)
        {
            return string.Empty;
        }

        public override void HandleAsInApp(NotificationBase notification)
        {
            if (notification is ChatNotification chatNotification)
            {
                _notificationsService.HandleNotification(chatNotification);
            }
        }

        public override void HandleAsRemote(NotificationBase notification)
        {
            if (notification is ChatNotification chatNotification)
            {
                Messenger.Default.Send(new OpenChatMessage(chatNotification.ChannelId));
            }
        }
    }
}
