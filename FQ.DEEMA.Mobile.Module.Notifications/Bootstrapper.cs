﻿using Autofac;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using FQ.DEEMA.Mobile.Module.Notifications.Services.Remote;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using FQ.DEEMA.Mobile.Module.Notifications.Handlers;

namespace FQ.DEEMA.Mobile.Module.Notifications
{
    public static class Bootstrapper
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<RemoteNotificationsService>().As<IRemoteNotificationsService>()
                .InstancePerDependency();
            containerBuilder.RegisterType<NotificationsManager>().As<INotificationsManager>().InstancePerDependency();

            containerBuilder.RegisterType<ChatNotificationProcessor>().As<INotificationProcessor>().InstancePerLifetimeScope();
            containerBuilder.RegisterType<NewsNotificationProcessor>().As<INotificationProcessor>().InstancePerLifetimeScope();

            // ViewModels
            containerBuilder.RegisterType<NotificationsViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NotificationsRootFrameViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NotificationsSettingsViewModel>().InstancePerDependency();
        }
    }
}