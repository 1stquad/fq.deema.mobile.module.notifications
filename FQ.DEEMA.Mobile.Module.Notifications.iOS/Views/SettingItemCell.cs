﻿using System;
using Foundation;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.Views
{
    internal partial class SettingItemCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("SettingItemCell");
        public static readonly UINib Nib;

        private bool _initialized;
        private Binding _isEnabledBinding;
        private WeakReferenceEx<NotificationSettingViewModel> _viewModelRef;

        static SettingItemCell()
        {
            Nib = UINib.FromName("SettingItemCell", NSBundle.MainBundle);
        }

        protected SettingItemCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        internal void Bind(NotificationSettingViewModel item)
        {
            if (!_initialized)
            {
                NameLabel.Font = StyleHelper.Style.BigFont;
                NameLabel.TextColor = StyleHelper.Style.TextNormalColor;
                EnabledSwitch.OnTintColor = StyleHelper.Style.AccentColor;
                Separator.BackgroundColor = StyleHelper.Style.SeparatorColor;
                _initialized = true;
            }

            NameLabel.Text = item.DisplayName;

            _viewModelRef = WeakReferenceEx.Create(item);

            _isEnabledBinding?.Detach();
            _isEnabledBinding = this.SetBinding(() => _viewModelRef.Target.IsEnabled, () => EnabledSwitch.On,
                BindingMode.TwoWay);
        }
    }
}