﻿using System;
using Foundation;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.Views
{
    internal partial class SettingSectionHeader : UITableViewHeaderFooterView
    {
        public static readonly NSString Key = new NSString("SettingSectionHeader");
        public static readonly UINib Nib;

        static SettingSectionHeader()
        {
            Nib = UINib.FromName("SettingSectionHeader", NSBundle.MainBundle);
        }

        protected SettingSectionHeader(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        internal void Bind(string text, UIColor separatorColor)
        {
            NameLabel.Font = StyleHelper.Style.SmallFont;
            NameLabel.TextColor = StyleHelper.Style.TextSecondaryColor;
            NameLabel.Text = text;

            Separator.BackgroundColor = separatorColor;
        }
    }
}