﻿using System;
using Foundation;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.Common.iOS.Helpers;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.Views
{
    public partial class NotificationItemCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("NotificationItemCell");
        public static readonly UINib Nib;
        private ShortDateTimeToStringConverter _converter;

        private bool _isInitialized;

        static NotificationItemCell()
        {
            Nib = UINib.FromName("NotificationItemCell", NSBundle.MainBundle);
        }

        protected NotificationItemCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
        
        internal void Bind(NotificationViewModel item, bool isLastItem)
        {
            if (!_isInitialized)
            {
                NameLabel.Font = StyleHelper.Style.NormalFont;
                NameLabel.TextColor = StyleHelper.Style.TextNormalColor;

                TimeLabel.Font = StyleHelper.Style.SmallFont;
                TimeLabel.TextColor = StyleHelper.Style.TextSecondaryColor;

                SeparatorView.BackgroundColor = StyleHelper.Style.SeparatorColor;
                AltSeparatorView.BackgroundColor = StyleHelper.Style.SeparatorColor;

                _converter = new ShortDateTimeToStringConverter(LocaleHelper.Is24HourFormat);

                _isInitialized = true;
            }

            SeparatorView.Hidden = isLastItem;
            AltSeparatorView.Hidden = !isLastItem;

            NameLabel.Text = item.Title;
            TimeLabel.Text = _converter.ConvertValue(item.Notification.Created);
        }
    }
}