// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.Views
{
	[Register ("SettingItemCell")]
	partial class SettingItemCell
	{
		[Outlet]
		UIKit.UISwitch EnabledSwitch { get; set; }

		[Outlet]
		UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		UIKit.UIView Separator { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EnabledSwitch != null) {
				EnabledSwitch.Dispose ();
				EnabledSwitch = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (Separator != null) {
				Separator.Dispose ();
				Separator = null;
			}
		}
	}
}
