﻿using Autofac;
using FQ.DEEMA.Mobile.Module.Notifications.iOS.Services;
using FQ.DEEMA.Mobile.Module.Notifications.Services;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS
{
    public static class BootstrapperIos
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            Bootstrapper.Configure(containerBuilder);

            containerBuilder.RegisterType<NotificationsService>().As<INotificationsService>().InstancePerDependency();
        }
    }
}