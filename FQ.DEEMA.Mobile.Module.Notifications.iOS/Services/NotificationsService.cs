﻿using CoreGraphics;
using FQ.DEEMA.Mobile.Module.Notifications.Messages;
using FQ.DEEMA.Mobile.Module.Notifications.Models;
using FQ.DEEMA.Mobile.Module.Notifications.Models.Notifications;
using FQ.DEEMA.Mobile.Module.Notifications.Services;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.iOS.Navigation;
using Softeq.XToolkit.WhiteLabel.Messenger;
using ToastBindings;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.Services
{
    public class NotificationsService : INotificationsService
    {
        private const int Margin = 16;
        private const int ExtraMargin = Margin + Margin + ImageSize;
        private const int ImageTopMargin = 20;
        private const int ImageSize = 17;

        private readonly IViewLocator _viewLocator;

        public NotificationsService(IViewLocator viewLocator)
        {
            _viewLocator = viewLocator;
        }

        //TODO review <In> Type
        public void HandleNotification(NewsNotification notification)
        {
            var topViewController = _viewLocator.GetTopViewController();
            if (topViewController == null)
            {
                return;
            }

            CreateNotificationView(
                notification.DisplayText,
                new RelayCommand(() => Messenger.Default.Send(new OpenArticleMessage(notification.ArticleId))),
                topViewController.View);
        }

        public void HandleNotification(ChatNotification notification)
        {
            var topViewController = _viewLocator.GetTopViewController();
            if (topViewController == null)
            {
                return;
            }

            CreateNotificationView(
                notification.DisplayText,
                new RelayCommand(() => Messenger.Default.Send(new OpenChatMessage(notification.ChannelId))),
                topViewController.View);
        }

        private static void HideToast((UIView View, UIView Toast) value)
        {
            ToastService.HideToast(value.View, value.Toast);
        }

        private static void CreateNotificationView(string text, RelayCommand openCommand, UIView rootView)
        {
            var view = new UIView
            {
                BackgroundColor = StyleHelper.Style.NotificationBackgroundColor
            };

            //button
            var openButton = UIButton.FromType(UIButtonType.System);
            openButton.TranslatesAutoresizingMaskIntoConstraints = false;
            view.AddSubview(openButton);
            NSLayoutConstraint.ActivateConstraints(new[]
            {
                openButton.LeftAnchor.ConstraintEqualTo(view.LeftAnchor),
                openButton.RightAnchor.ConstraintEqualTo(view.RightAnchor, -ExtraMargin),
                openButton.TopAnchor.ConstraintEqualTo(view.TopAnchor),
                openButton.BottomAnchor.ConstraintEqualTo(view.BottomAnchor)
            });
            openButton.SetCommand(openCommand);

            //initialize label
            var label = new UILabel
            {
                Text = text,
                Lines = 0,
                TextColor = StyleHelper.Style.ContentColor,
                Font = StyleHelper.Style.BigFont,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.AddSubview(label);
            NSLayoutConstraint.ActivateConstraints(new[]
            {
                label.LeftAnchor.ConstraintEqualTo(view.LeftAnchor, Margin),
                label.RightAnchor.ConstraintEqualTo(view.RightAnchor, -ExtraMargin),
                label.CenterYAnchor.ConstraintEqualTo(view.CenterYAnchor)
            });

            //initialize close button
            var closeButton = UIButton.FromType(UIButtonType.System);
            closeButton.TranslatesAutoresizingMaskIntoConstraints = false;
            view.AddSubview(closeButton);
            NSLayoutConstraint.ActivateConstraints(new[]
            {
                closeButton.RightAnchor.ConstraintEqualTo(view.RightAnchor),
                closeButton.TopAnchor.ConstraintEqualTo(view.TopAnchor),
                closeButton.BottomAnchor.ConstraintEqualTo(view.BottomAnchor),
                closeButton.WidthAnchor.ConstraintEqualTo(ExtraMargin)
            });
            closeButton.SetCommand(new RelayCommand<(UIView View, UIView toast)>(HideToast), (rootView, view));

            //initialize close image
            var imageView = new UIImageView
            {
                Image = UIImage.FromBundle(StyleHelper.Style.CancelBoundleName),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.AddSubview(imageView);
            NSLayoutConstraint.ActivateConstraints(new[]
            {
                imageView.RightAnchor.ConstraintEqualTo(view.RightAnchor, -Margin),
                imageView.TopAnchor.ConstraintEqualTo(view.TopAnchor, ImageTopMargin),
                imageView.HeightAnchor.ConstraintEqualTo(ImageSize),
                imageView.WidthAnchor.ConstraintEqualTo(ImageSize)
            });

            var size = label.SizeThatFits(new CGSize(rootView.Bounds.Width - Margin - ExtraMargin, double.MaxValue));
            view.Frame = new CGRect(0, 0, rootView.Bounds.Width, size.Height + Margin + Margin);

            ToastService.ShowToast(rootView, view);
        }
    }
}