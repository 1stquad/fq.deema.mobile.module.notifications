using System;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.iOS.Helpers;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS
{
    internal static class StyleHelper
    {
        private static readonly Lazy<INotificationsIosStyle> StyleLazy = Dependencies.IocContainer.LazyResolve<INotificationsIosStyle>();

        public static INotificationsIosStyle Style => StyleLazy.Value;
    }

    public interface INotificationsIosStyle
    {
        UIColor NotificationBackgroundColor { get; }
        UIColor ContentColor { get; }
        UIColor AccentColor { get; }
        UIColor TextNormalColor { get; }
        UIColor SeparatorColor { get; }
        UIColor TextSecondaryColor { get; }
        UIFont BigFont { get; }
        UIFont BigSemiboldFont { get; }
        UIFont NormalFont { get; }
        UIFont SmallFont { get; }

        string CancelBoundleName { get; }
        string BackBoundleName { get; }
        string TitleImageBoundleName { get; }
        string PlaceholderBoundleName { get; }

        AvatarImageHelpers.AvatarStyles ProfileAvatarStyles { get; }
    }
}