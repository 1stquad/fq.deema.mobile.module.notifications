﻿using System;
using UIKit;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Bindings.iOS;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.EventArguments;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS.Shared.Extensions;
using FQ.DEEMA.Mobile.Module.Notifications.iOS.Views;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.ViewControllers
{
    public partial class NotificationsViewController : ViewControllerBase<NotificationsViewModel>
    {
        private ObservableGroupTableViewSource<string, NotificationViewModel> _dataSource;
        private UIRefreshControl _refreshControl;

        public NotificationsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            View.BackgroundColor = StyleHelper.Style.ContentColor;

            _refreshControl = new UIRefreshControl();
            _refreshControl.SetCommand(nameof(_refreshControl.ValueChanged), ViewModel.RefreshCommand);
            _refreshControl.TintColor = StyleHelper.Style.AccentColor;

            TableView.RegisterNibForCellReuse(NotificationItemCell.Nib, NotificationItemCell.Key);
            TableView.RegisterNibForHeaderFooterViewReuse(SettingSectionHeader.Nib, SettingSectionHeader.Key);
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.BackgroundColor = UIColor.Clear;

            _dataSource = new ObservableGroupTableViewSource<string, NotificationViewModel>(
                TableView,
                ViewModel.Notifications,
                (view, model, items, indexPath) =>
                {
                    var cell = (NotificationItemCell)view.DequeueReusableCell(NotificationItemCell.Key, indexPath);
                    var isLast = indexPath.Row == items.Count - 1;
                    cell.Bind(model, isLast);
                    return cell;
                },
                (view, s) =>
                {
                    var cell = (SettingSectionHeader)view.DequeueReusableHeaderFooterView(SettingSectionHeader.Key);
                    cell.Bind(s, StyleHelper.Style.ContentColor);
                    return cell;
                })
            {
                HeightForHeader = 60f
            };
            _dataSource.SetCommandWithArgs(
                nameof(_dataSource.ItemSelected),
                new RelayCommand<GenericEventArgs<NotificationViewModel>>(x => { x.Value.OpenArticle(); }));
            _dataSource.SetCommand(
                nameof(_dataSource.LastItemRequested),
                new RelayCommand(OnLastItemRequested));

            TableView.Source = _dataSource;
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.AddSubview(_refreshControl);

            CustomNavigationItemView.AddTitleView(new UIImageView
            {
                Image = UIImage.FromBundle(StyleHelper.Style.TitleImageBoundleName)
            });
            CustomNavigationItemView.SetSettingsButtonAsync(ViewModel.UserName, ViewModel.UserPhotoUrl, ViewModel.RightCommand,
                StyleHelper.Style.ProfileAvatarStyles);

            BusyIndicator.Color = StyleHelper.Style.AccentColor;

            EmptyPlaceholder.Hidden = true;

            EmptyPlaceholderImage.Image = UIImage.FromBundle(StyleHelper.Style.PlaceholderBoundleName);

            EmptyPlaceholderText.Text = ViewModel.NotificationsLocalizedStrings.NoNotifications;
            EmptyPlaceholderText.Font = StyleHelper.Style.BigSemiboldFont;
            EmptyPlaceholderText.TextColor = StyleHelper.Style.TextSecondaryColor;
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
            {
                if (ViewModel.IsBusy)
                    BusyIndicator.StartAnimating();
                else
                    BusyIndicator.StopAnimating();
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsRefreshing).WhenSourceChanges(() =>
            {
                if (!ViewModel.IsRefreshing) _refreshControl.EndRefreshing();
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.HasData).WhenSourceChanges(() =>
            {
                EmptyPlaceholder.Hidden = ViewModel.HasData;
            }));
        }

        private void OnLastItemRequested()
        {
            ViewModel.LoadNextPage();
        }
    }
}