﻿using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.WhiteLabel.iOS.Navigation;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.ViewControllers
{
    public class NotificationsRootFrameViewController : RootFrameNavigationControllerBase<NotificationsRootFrameViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationBar.Hidden = true;
        }
    }
}