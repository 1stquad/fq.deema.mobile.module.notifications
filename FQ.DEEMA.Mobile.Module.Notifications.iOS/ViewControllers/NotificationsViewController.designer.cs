// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.ViewControllers
{
	[Register ("NotificationsViewController")]
	partial class NotificationsViewController
	{
		[Outlet]
		UIKit.UIActivityIndicatorView BusyIndicator { get; set; }

		[Outlet]
		UIKit.UINavigationItem CustomNavigationItemView { get; set; }

		[Outlet]
		UIKit.UIView EmptyPlaceholder { get; set; }

		[Outlet]
		UIKit.UIImageView EmptyPlaceholderImage { get; set; }

		[Outlet]
		UIKit.UILabel EmptyPlaceholderText { get; set; }

		[Outlet]
		UIKit.UITableView TableView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BusyIndicator != null) {
				BusyIndicator.Dispose ();
				BusyIndicator = null;
			}

			if (CustomNavigationItemView != null) {
				CustomNavigationItemView.Dispose ();
				CustomNavigationItemView = null;
			}

			if (TableView != null) {
				TableView.Dispose ();
				TableView = null;
			}

			if (EmptyPlaceholder != null) {
				EmptyPlaceholder.Dispose ();
				EmptyPlaceholder = null;
			}

			if (EmptyPlaceholderImage != null) {
				EmptyPlaceholderImage.Dispose ();
				EmptyPlaceholderImage = null;
			}

			if (EmptyPlaceholderText != null) {
				EmptyPlaceholderText.Dispose ();
				EmptyPlaceholderText = null;
			}
		}
	}
}
