﻿using System;
using Foundation;
using FQ.DEEMA.Mobile.Module.Notifications.iOS.Views;
using FQ.DEEMA.Mobile.Module.Notifications.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Bindings.iOS;
using Softeq.XToolkit.Common.Collections;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.Notifications.iOS.ViewControllers
{
    public partial class NotificationsSettingsViewController : ViewControllerBase<NotificationsSettingsViewModel>
    {
        public NotificationsSettingsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            BusyIndicator.Color = StyleHelper.Style.AccentColor;
            BusyIndicator.HidesWhenStopped = true;

            CustomNavigationItem.SetCommand(UIImage.FromBundle(StyleHelper.Style.BackBoundleName), ViewModel.BackCommand, true);
            CustomNavigationItem.Title = ViewModel.Title;

            TableView.RegisterNibForCellReuse(SettingItemCell.Nib, SettingItemCell.Key);
            TableView.RegisterNibForHeaderFooterViewReuse(SettingSectionHeader.Nib, SettingSectionHeader.Key);
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            var source = new NotificationsTableViewSource(ViewModel.Settings, TableView);

            TableView.Source = source;
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
            {
                if (ViewModel.IsBusy)
                {
                    BusyIndicator.StartAnimating();
                }
                else
                {
                    BusyIndicator.StopAnimating();
                }
            }));
        }

        private class NotificationsTableViewSource : ObservableGroupTableViewSource<
            ObservableKeyGroup<string, NotificationSettingViewModel>>
        {
            public NotificationsTableViewSource(
                ObservableRangeCollection<ObservableKeyGroup<string, NotificationSettingViewModel>> dataSource,
                UITableView tableView) : base(dataSource, tableView, null, null, null)
            {
                HeightForRow = 50;
                HeightForHeader = 60;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                var cell = tableView.DequeueReusableCell(SettingItemCell.Key);
                var item = DataSource[indexPath.Section][indexPath.Row];

                var settingItemCell = (SettingItemCell) cell;
                settingItemCell.Bind(item);

                return settingItemCell;
            }

            public override nint NumberOfSections(UITableView tableView)
            {
                return DataSource.Count;
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return DataSource[(int) section].Count;
            }

            public override UIView GetViewForHeader(UITableView tableView, nint section)
            {
                var view = tableView.DequeueReusableHeaderFooterView(SettingSectionHeader.Key);

                var settingSectionHeader = (SettingSectionHeader) view;
                settingSectionHeader.Bind(DataSource[(int) section].Key, StyleHelper.Style.SeparatorColor);

                return settingSectionHeader;
            }
        }
    }
}